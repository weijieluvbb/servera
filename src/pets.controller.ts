import {
  Controller,
  Get,
  Head,
  HttpService
} from "@nestjs/common";

@Controller()
export class PetsController {
  constructor(private readonly httpService: HttpService) {}

  @Get("/cats")
  async getCats() {
    const { data } = await this.httpService.get(process.env.CATS_URI).toPromise();
    
    return data
  }

  @Get("/dogs")
  async getDogs() {
    const { data } = await this.httpService.get(process.env.DOGS_URI).toPromise();

    return data
  }

  @Head("/health")
  async health() {
    return 'healthy app!!'
  }
}
