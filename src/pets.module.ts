import { Module, HttpModule } from "@nestjs/common";
import { PetsController } from "./pets.controller";

@Module({
  imports: [
    HttpModule
  ],
  controllers: [PetsController],
})
export class PetsModule {}
