# Setup E2E tests with GitLab CI
A boilerplate with a NestJS app that proxies to 2 microservices. The boilerplate is part of a [medium article](https://medium.com/@datails/end-to-end-test-microservices-using-docker-and-gitlab-ci-53119c2fad89) about end-to-end test microservices.


## Working
Exposes 1 api on `http://localhost:3000`.

## Start the app

```bash
docker-compose up
```

### Get a dog
```bash
curl http://localhost:3000/dogs
```

### Get a cat
```bash
curl http://localhost:3000/cats
```